var mongoose = require('mongoose');

var CarSchema = new mongoose.Schema({
  isbn: String,
  carname: String,
  carbrand: String,
  description: String,
  creation_year: String,
  available: Boolean,
  updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Car', CarSchema);

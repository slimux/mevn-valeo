var mongoose = require('mongoose');
var Double = require('mongoose-double')(mongoose);
var LocationSchema = new mongoose.Schema({
  latitude: {
    type: Double,
    required: true,
  },
  longitude: {
    type: Double,
    required: true,
  },
  car: {
    type: String,
    required: true,
  },
  date:{
    type:Date,
    default:Date.now
  }
});
var Location = mongoose.model('Location', LocationSchema);
module.exports = Location;

import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import RegisterView from './components/Register.vue'

import NotFoundView from './components/404.vue'

// Import Views - Dash
import DashboardView from './components/views/Dashboard.vue'
import TasksView from './components/views/Tasks.vue'
import SettingView from './components/views/Setting.vue'
import AccessView from './components/views/Access.vue'
import ServerView from './components/views/Server.vue'
import ReposView from './components/views/Repos.vue'
import TablesView from './components/views/Tables'
import DeliveriesView from './components/views/Deliveries'
import ShowDeliveryView from './components/views/showDelivery'
import AddDeliveryView from './components/views/addDelivery'
import TrackerView from './components/views/Tracker'
import LiveTrackingView from './components/views/LiveTracking'
import CustomTrackingView from './components/views/CustomTracking'

import EmployeeList from '@/components/views/employee/EmployeeList'
import ShowEmployee from '@/components/views/employee/ShowEmployee'
import CreateEmployee from '@/components/views/employee/CreateEmployee'
import EditEmployee from '@/components/views/employee/EditEmployee'

import CarList from '@/components/views/car/CarList'
import ShowCar from '@/components/views/car/ShowCar'
import CreateCar from '@/components/views/car/CreateCar'
import EditCar from '@/components/views/car/EditCar'
import OB from '@/components/views/car/OB'
import CarListAll from '@/components/views/car/CarListAll'
import CarsSound from '@/components/views/car/CarsSound'
import CarListAll1 from '@/components/views/car/CarListAll1'


import ObstacleList from '@/components/views/obstacle/ObstacleList'
import ShowObstacle from '@/components/views/obstacle/ShowObstacle'

// Routes
const routes = [
  {
    path: '/login',
    component: LoginView
  },
  {
    path: '/tracker',
    component: TrackerView
  },
  {
    path: '/register',
    component: RegisterView
  },
  {
    path: '/',
    component: DashView,
    children: [
      {
        path: 'obstacle-list',
        name: 'ObstacleList',
        component: ObstacleList
      },
      {
        path: 'show-obstacle/:id',
        name: 'ShowObstacle',
        component: ShowObstacle
      },
      {
        path: 'list',
        name: 'CarListAll',
        component: CarListAll
      },
      {
        path: 'listA',
        name: 'CarListAll1',
        component: CarListAll1
      },
      {
        path: 'list-cars',
        name: 'CarList',
        component: CarList
      },
      {
        path: 'sound/:id',
        name: 'CarsSound',
        component: CarsSound
      },
      {
        path: 'ob',
        name: 'OB',
        component: OB
      },
      {
        path: 'show-car/:id',
        name: 'ShowCar',
        component: ShowCar
      },
      {
        path: '/add-car',
        name: 'CreateCar',
        component: CreateCar
      },
      {
        path: 'edit-car/:id',
        name: 'EditCar',
        component: EditCar
      },
      {
        path: '/list-employees',
        name: 'EmployeeList',
        component: EmployeeList
      },
      {
        path: '/show-employee/:id',
        name: 'ShowEmployee',
        component: ShowEmployee
      },
      {
        path: '/add-employee',
        name: 'CreateEmployee',
        component: CreateEmployee
      },
      {
        path: '/edit-employee/:id',
        name: 'EditEmployee',
        component: EditEmployee
      },
      {
        path: 'dashboard',
        alias: '',
        component: DashboardView,
        name: 'Dashboard',
        meta: {description: 'Overview of environment'}
      },
      {
        path: '/live-tracking',
        alias: '',
        component: LiveTrackingView,
        name: 'Live Tracking',
        meta: {description: 'Overview of environment'}
      },
      {
        path: '/custom-tracking',
        alias: '',
        component: CustomTrackingView,
        name: 'Custom Tracking',
        meta: {description: 'Overview of environment'}
      },
      {
        path: 'deliveries',
        alias: '',
        component: DeliveriesView,
        name: 'Deliveries',
        meta: {description: 'Overview of environment'},
      },
      {
        path:'deliveries/add',
        alias:'',
        component:AddDeliveryView,
        name:'adddelivery',
        meta: {description: 'Overview of environment'},
      },
      {
        path:'deliveries/:id',
        alias:'',
        component:ShowDeliveryView,
        name:'showdelivery',
        meta: {description: 'Overview of environment'},
      },
      {
        path: 'tables',
        alias: '',
        component: TablesView,
        name: 'tables',
        meta: {description: 'Overview of environment'}
      },{
        path: 'tasks',
        component: TasksView,
        name: 'Tasks',
        meta: {description: 'Tasks page in the form of a timeline'}
      }, {
        path: 'setting',
        component: SettingView,
        name: 'Settings',
        meta: {description: 'User settings page'}
      }, {
        path: 'access',
        component: AccessView,
        name: 'Access',
        meta: {description: 'Example of using maps'}
      }, {
        path: 'server',
        component: ServerView,
        name: 'Servers',
        meta: {description: 'List of our servers', requiresAuth: true}
      }, {
        path: 'repos',
        component: ReposView,
        name: 'Repository',
        meta: {description: 'List of popular javascript repos'}
      }
    ]
  }, {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes

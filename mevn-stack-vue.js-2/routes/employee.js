var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Employee = require('../models/employee.js');

/* GET ALL Employees */
router.get('/', function(req, res, next) {
  Employee.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET SINGLE Employee BY ID */
router.get('/:id', function(req, res, next) {
  Employee.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.status(200).json(post);
  });
});

/* SAVE Employee */
router.post('/', function(req, res, next) {
  Employee.create(req.body, function (err, post) {
    if (err) {
      res.send(err)
    }
    res.json(post);
  });
});

/* UPDATE Employee */
router.put('/:id', function(req, res, next) {
  console.log(req.body);
  Employee.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE Employee */
router.delete('/:id', function(req, res, next) {
  Employee.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;

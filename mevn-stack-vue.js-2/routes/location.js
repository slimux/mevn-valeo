var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Location = require('../models/LocationSchema.js');

/* add */
/* get */
router.get('/:lat/:long/:car', function(req, res, next) {
  var location=new Location({
    latitude : req.params.lat,
    longitude : req.params.long,
    car : req.params.car
  });

  location.save(function (err,location) {
    if(err)
      res.send(err);
    else{
      res.send(location);
    }

  });

});

/* findByDateAndCar */
/* get */
router.get('/custom/:startDate/:endDate/:car', function(req, res, next) {
  console.log('custom')

  Location.find({ date: { $gte: new Date(req.params.startDate),$lte:new Date(req.params.endDate) },car:req.params.car},function (err,locations) {
    if(err) {
      res.send(err)
      console.log('err')
    }
    if(!locations) {
      res.status(404).send();
      console.log('locations')
    }
    else {
      res.json(locations);
      console.log('json')
    }

  });
});



module.exports = router;

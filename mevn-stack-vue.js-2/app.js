var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var expressVue = require("express-vue");





const expressVueMiddleware = expressVue.init();


var car = require('./routes/car');
var user = require('./routes/user');
var delivery = require('./routes/delivery');
var location = require('./routes/location');
var employee = require('./routes/employee');
var obstacle = require('./routes/obstacle');


var app = express();
app.use(expressVueMiddleware);
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
//mongoose.connect('mongodb://localhost/mevn-stack', { useMongoClient: true, promiseLibrary: require('bluebird') })
mongoose.connect('mongodb://mahdi:j1ofjVuh30NKD87I@mevn-stack-shard-00-00-spkmy.mongodb.net:27017,mevn-stack-shard-00-01-spkmy.mongodb.net:27017,mevn-stack-shard-00-02-spkmy.mongodb.net:27017/mevn-stack?ssl=true&replicaSet=mevn-stack-shard-0&authSource=admin')
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));

var db = mongoose.connection;




//use sessions for tracking logins
app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
app.use(express.static(path.join(__dirname, 'dist')));
app.set('view engine', 'html');
app.use('/cars', express.static(path.join(__dirname, 'dist')));
app.use('/car', car);
app.use('/user',user);
app.use('/delivery',delivery);
app.use('/location',location);
app.use('/employees',employee);
app.use('/obstacle', obstacle);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json('error');
});

module.exports = app;

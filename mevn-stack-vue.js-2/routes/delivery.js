var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Delivery = require('../models/DeliverySchema.js');

/* get all */
/* get */
router.get('/', function(req, res, next) {

  Delivery.find(function (err,deliverys) {
    if(err) {
      res.send(err)
      console.log('err')
    }
    if(!deliverys) {
      res.status(404).send();
      console.log('users')
    }
    else {
      res.json(deliverys);
      console.log('json')
    }

  });
});

/* add */
/* post */
router.post('/', function(req, res, next) {
  console.log(req.body);
  var delivery=new Delivery(req.body);
  delivery.save(function (err,deliverys) {
    if(err)
      res.send(err);
    else
      res.json(deliverys);
  });

});

/* update*/
/*put*/
router.put('/:id', function(req, res){
  Delivery.findByIdAndUpdate(req.params.id,{$set:req.body}, function(err, result){
    if(err){
      console.log(err);
    }
    res.json(result)
  });
});

/*delete*/
router.delete('/:id',function (req,res) {

  var id=req.params.id;
  Delivery.findByIdAndRemove(id,function (err,delivery) {
    if(err)
      res.send(err);
    else
      res.send();
  });

});

/*get by id*/
router.get('/:id',function (req,res) {
  var id=req.params.id;
  Delivery.findById(id).exec(function (err,delivery) {
    if(err) {
      res.send(err)

    }
    if(!delivery) {
      res.status(404).send();

    }
    else {
      res.json(delivery);

    }
  });
});



module.exports = router;

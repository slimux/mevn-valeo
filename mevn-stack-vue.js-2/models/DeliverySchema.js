var mongoose = require('mongoose');
var Double = require('mongoose-double')(mongoose);
var bcrypt = require('bcrypt');
var DeliverySchema = new mongoose.Schema({
  customer : {
    fullName: {
      type:String,
    },
    address: {
      street: {
        type:String,
      },
      city:{
        type:String,
      },
      town: {
        type:String,
      },
      country: {
        type:String,
      },
      postalCode: {
        type:Number,
      },
      latitude: {
        type:Double,
      },
      longitude:{
        type:Double,
      }
    }
  },
  order: {
    type:String,
    required:true
  },
  price: {
    type:Double,
    required:true
  },
  dateStart:{
    type:Date,
    default:Date.now
  },
  dateDeliver:{
    type:Date,
    default:Date.now
  },
  employee:{
    type:String
  },
  car:{
    type:String
  }
});

var Delivery = mongoose.model('Delivery', DeliverySchema);
module.exports = Delivery;
